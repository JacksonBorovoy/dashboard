<?php


namespace App\Http\Controllers;


use Carbon\Carbon;
use Faker\Provider\DateTime;
use Spatie\GoogleCalendar\Event;

class CalendarController extends Controller
{
    private $hourStartTimeStamp;
    private $hourEndTimeStamp;

    public function index()
    {
        $data = [];

        $this->hourStartTimeStamp = $this->roundTime(time(), 15);
        $objDateTime = new \DateTime();
        $objDateTime->setTimestamp($this->hourStartTimeStamp);
        $isoHourStart = $objDateTime->format(\DateTime::ISO8601);
        $this->hourEndTimeStamp = $this->roundTime(mktime(date("H", strtotime("+4 hours")), date("i"), 0), 15);
        $objDateTime->setTimestamp($this->hourEndTimeStamp);
        $isoHourEnd = $objDateTime->format(\DateTime::ISO8601);
        $startDateTime = new Carbon($isoHourStart);
        $endDateTime = new Carbon($isoHourEnd);
        $redRoom = Event::get($startDateTime, $endDateTime, [],env('RED_ROOM'))->toArray();
        $greenRoom = Event::get($startDateTime, $endDateTime, [], env('GREEN_ROOM'))->toArray();
        $yellowRoom = Event::get($startDateTime, $endDateTime, [], env('YELLOW_ROOM'))->toArray();
        $data['room']['red'] = $this->loadingRoom($redRoom);
        $data['room']['green'] = $this->loadingRoom($greenRoom);
        $data['room']['yellow'] = $this->loadingRoom($yellowRoom);
        $data['time'][] = $objDateTime->setTimestamp($this->hourStartTimeStamp)->format('H');
        $data['time'][] = $objDateTime->setTimestamp($this->hourEndTimeStamp)->format('H');
        return view('dashboard', ['data' => $data]);
    }

    private function loadingRoom(array $event)
    {
        $data = [];
        $eventTime = [];
        if(!empty($event)) {
            if(strtotime($event[0]->startDateTime) > $this->hourStartTimeStamp) {
                $eventTime[] = (new \DateTime())->setTimestamp($this->hourStartTimeStamp)->format(\DateTime::ISO8601);
                $flag = false;
            } else {
                $event[0]->startDateTime = new Carbon((new \DateTime())->setTimestamp($this->hourStartTimeStamp)->format(\DateTime::ISO8601));
                $flag = true;
            }
            if(strtotime($event[count($event) - 1]->endDateTime) > $this->hourEndTimeStamp) {
                $event[count($event) - 1]->endDateTime = new Carbon((new \DateTime())->setTimestamp($this->hourEndTimeStamp)->format(\DateTime::ISO8601));
            }
            foreach($event as $key => $item) {
                $eventTime[] = $item->start->dateTime;
                $eventTime[] = $item->end->dateTime;
            }
            if(strtotime($event[count($event) - 1]->endDateTime) < $this->hourEndTimeStamp) {
                $eventTime[] = (new \DateTime())->setTimestamp($this->hourEndTimeStamp)->format(\DateTime::ISO8601);
            }
            for($i=0; $i < count($eventTime)-1; $i++) {
                $result = $this->getTimeBlock($eventTime[$i], $eventTime[$i+1], $flag);
                $data = array_merge($data, $result);
                $flag = !$flag;
            }
        } else {
            $eventTime[] = (new \DateTime())->setTimestamp($this->hourStartTimeStamp)->format(\DateTime::ISO8601);
            $eventTime[] = (new \DateTime())->setTimestamp($this->hourEndTimeStamp)->format(\DateTime::ISO8601);
            $flag = false;
            for($i=0; $i < count($eventTime)-1; $i++) {
                $result = $this->getTimeBlock($eventTime[$i], $eventTime[$i+1], $flag);
                $data = array_merge($data, $result);
                $flag = !$flag;
            }
        }
        return $data;
    }

    private function getTimeBlock($start, $end, $flag) {
        $result = [];
        $rangeEvent = strtotime($end) - strtotime($start);
        $countBlock = $rangeEvent / 900;// 900s = 15min
        for($i=0; $i < $countBlock; $i++) {
            $result[] = $flag;
        }
        return $result;
    }

    private function roundTime($ts, $step) {
        return (floor(floor($ts / 60) / 60) * 3600 + floor(date("i", $ts) / $step) * $step * 60);
    }
}