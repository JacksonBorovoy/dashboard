<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = \Spatie\GoogleCalendar\Event::get();
    dd($data);
    return view('welcome');
});

Route::get('dashboard', 'CalendarController@index');
